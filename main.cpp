#include <iostream>
#include <restclient-cpp/restclient.h>
#include <nlohmann/json.hpp>
#include <random>
using json = nlohmann::json;

bool haveCaught(std::string newPokemon)
{
	RestClient::Response allPokemon = RestClient::get("http://127.0.0.1:5000/fetch/all"); // go to pokeComputer to look up what we have
	json allData = json::parse(allPokemon.body);										  // convert data from computer to readeble json format

	for (const auto &pokemon : allData) // see if we have caught it before
	{
		std::string data = pokemon["data"];
		json caughtPokemon = json::parse(data);

		if (newPokemon == caughtPokemon["name"])
		{
			return true;
		}
	}
	return false;
}

json getPokemonData(int number)
{
	RestClient::Response respons = RestClient::get("https://pokeapi.co/api/v2/pokemon/" + std::to_string(number) + "/"); // go to pokemon x and get everything about it
	json pokemonDataAll = json::parse(respons.body);																	 // cnovert the data to json format so we can tamper with it

	int types = pokemonDataAll["types"].size(); // get nr of types the pokemon has

	json pokemonDataModified =
		{
			{"name", pokemonDataAll["name"]},						 // add name to pokeDex
			{"sprites", pokemonDataAll["sprites"]["front_default"]}, // add image of pokemon to pokeDex
			{"types", {}},
		};

	for (int i = 0; i < types; i++) // find out if the pokemon has more than one type
	{
		pokemonDataModified["types"].push_back(pokemonDataAll["types"][i]["type"]["name"]); // fill inn the type to pokeDex
	}

	return pokemonDataModified;
}

int numberGenerator(int max)
{
	std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr(1, max);
    return distr(gen);
}

int main()
{

	int number = numberGenerator(898);	   // Generate random number to catch
	json Pokemon = getPokemonData(number); // encounter the wild pokemon!

	if (!haveCaught(Pokemon["name"])) // catch pokemon if we dont have it!
	{
		json DataToSend; // make a json that we later send to our pokeComputer (python server)
		DataToSend["meta"] = "pokemon";
		DataToSend["data"] = Pokemon.dump(); // convert data into a json that is ready for python sending

		RestClient::post("http://127.0.0.1:5000/submit", "application/json", DataToSend.dump()); // send data to python server

		std::cout << "You just cought a " << Pokemon["name"] << "!" << std::endl; // print in terminal that we cought a new pokemon
	}
	else
	{
		std::cout << "Already caught!" << std::endl; // aw damn, continue the search!
	}

	
}